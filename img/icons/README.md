# icons

This directory contains several icons that are licensed and used as follows:

| \#  | icons                                              | Author            | License                                                                       | GitHub URL                                                            |
|-----|----------------------------------------------------|-------------------|-------------------------------------------------------------------------------|-----------------------------------------------------------------------|
| 1   | `tick` (changed color from `#ffffff` to `#53bd00`) | Mariusz Ostrowski | [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)                     | [mariuszostrowski/subway](https://github.com/mariuszostrowski/subway) |
| 2   | `clipboard`, `clipboard-tick`                      | Jay Newey         | [MIT](https://raw.githubusercontent.com/jaynewey/charm-icons/main/LICENSE)    | [jaynewey/charm-icons](https://github.com/jaynewey/charm-icons)       |
| 2   | `copy`                                             | Arturo Wibawa     | [MIT](https://raw.githubusercontent.com/artcoholic/akar-icons/master/LICENSE) | [artcoholic/akar-icons](https://github.com/artcoholic/akar-icons)     |
| 3   | `tash-outline`                                     | Ionic             | [MIT](https://raw.githubusercontent.com/ionic-team/ionicons/main/LICENSE)     | [ionic-team/ionicons](https://github.com/ionic-team/ionicons)         |

