const cacheName = 'qreader';
const staticAssets = [
    // App
    './',
    // './index.html',
    './css/style.css',
    './img/icons/clipboard.svg',
    './img/icons/clipboard-tick.svg',
    './img/icons/copy.svg',
    './img/icons/tick.svg',
    './img/icons/trash-outline.svg',
    // './js/app.js',
    // './js/index.js',
    './site.webmanifest',

    // Third party libs

    // Bootstrap
    './css/3rdparty/bootstrap.min.css',
    './css/3rdparty/bootstrap.min.css.map',
    './js/3rdparty/bootstrap.bundle.min.js',
    './js/3rdparty/bootstrap.bundle.min.js.map',

    // jquery
    './js/3rdparty/jquery-3.6.0.min.js',

    // JsQRScanner
    './css/3rdparty/jsqrscanner.css',
    './js/3rdparty/jsqrscanner/88507C13C1223C3F2A335CFAAA4EF584.cache.js',
    './js/3rdparty/jsqrscanner/9C51964BB0BBCC41BB79120ED90449EA.cache.js',
    './js/3rdparty/jsqrscanner/B566A15506556F952CAD2B7994FFA824.cache.js',
    './js/3rdparty/jsqrscanner/D9940D84355A4C8E89013B8814821244.cache.js',
    './js/3rdparty/jsqrscanner/F4C3969B01AFD421179360B47BCEA2E0.cache.js',
    './js/3rdparty/jsqrscanner/jsqrscanner.nocache.js',
];

self.addEventListener('install', async () => {
    const cache = await caches.open(cacheName);
    await cache.addAll(staticAssets);
    return self.skipWaiting();
})

self.addEventListener('activate', () => {
    self.clients.claim().then(() => {
    });
})

self.addEventListener('fetch', async e => {
    const request = e.request;
    const url = new URL(request.url);

    if (url.origin === location.origin) {
        e.respondWith(cacheFirst(request));
    } else {
        e.respondWith(networkAndCache(request));
    }
});

async function cacheFirst(request) {
    const cache = await caches.open(cacheName);
    const cached = await cache.match(request);
    return cached || fetch(request);
}

async function networkAndCache(request) {
    const cache = await caches.open(cacheName);
    try {
        const fresh = await fetch(request);
        await cache.put(request, fresh.clone());
        return fresh;
    } catch (e) {
        return await cache.match(request);
    }
}
