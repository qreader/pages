let controller = undefined;

//function returning a promise with a video stream
function provideVideoQQ() {
    return navigator.mediaDevices.enumerateDevices()
        .then(function (devices) {
            let exCameras = [];
            devices.forEach(function (device) {
                if (device.kind === 'videoinput') {
                    exCameras.push(device.deviceId);
                }
            });

            return Promise.resolve(exCameras);
        }).then(function (ids) {
            if (ids.length === 0) {
                return Promise.reject('Could not find a webcam');
            }
            return navigator.mediaDevices.getUserMedia({
                video: {
                    facingMode: "environment",
                    aspectRatio: 1 // square camera window
                }
            });
        });
}

//this function will be called when JsQRScanner is ready to use
function JsQRScannerReady() {
    logInfo("Initializing JsQRScannerReady");

    controller = getController();
    controller.prepareUi();

    // create a new scanner passing to it a callback function that will be invoked when
    // the scanner successfully scans a QR code
    let jbScanner = new JsQRScanner(function (scannedText) {
        try {
            let result = controller.onCodeScan(scannedText);
        } catch (e) {
            logError(e);
        }
    }, provideVideoQQ);
    logInfo("Linked View.onCodeScan and provideVideoQQ");

    // Reduce the size of analyzed images to increase performance on mobile devices
    // const imageMaxSize = 300;
    // jbScanner.setSnapImageMaxSize(imageMaxSize);
    // logInfo("Reduced the size of analyzed images to " + imageMaxSize);

    let scannerParentElement = document.getElementById("scanner");
    let addendum = " jbScanner to element #" + scannerParentElement.id;
    if (scannerParentElement) {
        jbScanner.appendTo(scannerParentElement);
        logDebug("Appended" + addendum);
        logInfo("Successfully initialized JsQRscannerReady");
    } else {
        logError("Could not append" + addendum + ", element not found");
    }
}
