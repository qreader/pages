window.addEventListener('load', () => {
    registerServiceWorker().then(() => {
    });
})

async function registerServiceWorker() {
    if ('serviceWorker' in navigator) {
        try {
            await navigator.serviceWorker.register('./serviceworker.js')
        } catch (e) {
            console.error('Service worker registration failed: ' + e);
        }
    }
}

// function displayBlockWrap(id, show = true) {
//     let element = document.getElementById(id);
//     displayBlock(element, show);
//     return element;
// }
