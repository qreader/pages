class Model {
    constructor() {
        this._getStoredScanResults();
    }

    addScanResult(scannedText) {
        const scanResult = {
            id: this.scanResults.length > 0 ? this.scanResults[this.scanResults.length - 1].id + 1 : 1,
            dateTime: Date.now(),
            value: scannedText
        };

        this.scanResults.push(scanResult);
        this._commit(this.scanResults);
    }

    bindScanResultsChanged(callback) {
        this.onScanResultsChanged = callback;
    }

    // deleteScanResult(id) {
    //     this.scanResults = this.scanResults.filter(result => result.id !== id);
    //     this._commit(this.scanResults);
    // }

    _commit(scanResults) {
        this.onScanResultsChanged(scanResults);
        this._setStoredScanResults(scanResults);
    }

    _getStoredScanResults() {
        let cachedScanResult = JSON.parse(window.localStorage.getItem('scanResults')) || [];
        logInfo("Loaded " + cachedScanResult.length + " cached scan results");
        this.scanResults = cachedScanResult;
        return cachedScanResult;
    }

    _setStoredScanResults(object) {
        return window.localStorage.setItem('scanResults', JSON.stringify(object));
    }
}

class View {
    constructor(model) {
        this.model = model;

        // Hint
        this.hintPanel = document.getElementById('hint-panel');
        this.hintPrefixSpan = document.getElementById('hint-prefix');
        this.hintMsgSpan = document.getElementById('hint-msg');

        // Scan results
        // Recently scanned element
        this.scanTextDiv = document.getElementById('scan-text-div');
        this.lastScannedElement = document.getElementById('recent-scan');
        this.clipboardButton = document.getElementById('clipboard-button');

        // History
        this.scanHistoryDiv = document.getElementById('scan-history-div');
        this.scanHistoryElement = document.getElementById('scan-history');
        this.scanHistoryLabel = document.getElementById('scan-history-label');

        // Modal
        this.modalMsgHeader = document.getElementById('modal-msg-header');
        this.modalMsgUrl = document.getElementById('modal-msg-url');
        this.modalMsgFooter = document.getElementById('modal-msg-footer');
        this.interval = 0;

        this.debugMode = false;
        this.execTimerCallback = false;
        this.lastScannedText = '';
        this.showNavigateModal = false;
        this.showTitle = false;
        this.onCodeScanCallback = undefined;
        this.hasPermission = false;

        let self = this;
        let modalConfirm = function (callback) {
            $("#modal-btn-yes").on("click", function () {
                callback(true);
                self._showModal(false);
            });

            $("#modal-btn-no").on("click", function () {
                callback(false);
                self._showModal(false);
            });
        };
        modalConfirm(function (confirm) { // ignore confirm result
                self.showNavigateModal = false;
            }
        );
        this._updateDevicePermissions();
    }

    prepareUi(scanResults) {
        if (this.debugMode) {
            return;
        }

        let length = scanResults.length;
        if (length > 0) {
            this._updateHistoryLabel(length);
            this._displayBlock(this.scanHistoryDiv);
        }
    }

    copyToClipboard(value = null) {
        if (value == null) {
            value = this.lastScannedElement.value;
        }

        if (this.execTimerCallback) {
            return;
        }

        navigator.clipboard.writeText(value).then(function () {
            logInfo('Async: Copying to clipboard was successful!');
        }, function (e) {
            logError('Async: Could not copy text: ' + e);
        });

        this.showTitle = true;
        this._toggleClipboardTooltip();
        this.clipboardButton.classList.toggle("confirm");

        let self = this;
        this.interval = setInterval(function () {
            if (!self.execTimerCallback) {
                return;
            }
            self.clipboardButton.classList.toggle("confirm");
            self.execTimerCallback = false;
            self.clipboardButton.title = '';
            clearInterval(self.interval);
            self._toggleClipboardTooltip();
        }, 2000);

        this.execTimerCallback = true;
    }

    // bindDeleteScanResult(handler) {
    //     // this.todoList.addEventListener('click', event => {
    //     //     if (event.target.className === 'delete') {
    //     //         const id = parseInt(event.target.parentElement.id)
    //     //         handler(id);
    //     //     }
    //     // })
    // }

    bindAddScanResult(handler) {
        let self = this;
        this.onCodeScanCallback = event => {
            event.preventDefault();
            if (self.model.scanResults) {
                handler(self.model.scanResults);
            }
        }
    }

    clearHistoryElement() {
        this.scanHistoryElement.value = '';
    }

    displayScanResults(scanResults) {
        this.clearHistoryElement();
        for (let i = 0; i < scanResults.length; ++i) {
            this._appendToHistory(scanResults[i].value);
        }
        this._updateHistoryLabel(scanResults.length);
    }

    enableDebugMode(value = true) {
        this.debugMode = value;
        this._displayBlock(this.scanTextDiv);
        this._displayBlock(this.scanHistoryDiv);
    }

    onCodeScan(scannedText) {
        if (!this.hasPermission || this.debugMode) {
            if (!this.hasPermission) {
                this._updateDevicePermissions();
            }
            if (!this.debugMode) {
                logInfo("Entering debug mode");
                this.enableDebugMode();
            }
            return false;
        }
        if (this.showNavigateModal) {
            logInfo("Skipping scan while modal is open");
            return;
        }
        if (scannedText === undefined || scannedText.length === 0) {
            logDebug("onCodeScan(): scannedText = " + scannedText);
            logInfo("Probably no camera found")
            return false;
        }
        logDebug("scannedText = " + scannedText);

        if (this.lastScannedText == null) {
            this.lastScannedText = '';
        }
        let comparable = this.lastScannedText.length > 0 ? this.lastScannedText : this._getLastElementAdded();

        this._setScannedText(scannedText);
        if (comparable === scannedText) {
            this._displayBlock(this.scanTextDiv);
            logDebug("Skipping scan result, last scanned code has the same value");
            return false;
        }
        this._displayBlock(this.hintPanel, false);

        this._appendToHistory(scannedText);

        this._displayBlock(this.scanTextDiv);
        let historyCount = this.scanResults.length + 1;

        this._showUpdatedHistoryBlock(historyCount);
        this._showModalIfUrl(scannedText);

        return true;
    }

    setScanResults(scanResults) {
        this.scanResults = scanResults;
    }

    _appendToHistory(scannedText) {
        if (this.scanHistoryElement) {
            this.scanHistoryElement.value = scannedText + '\n' + this.scanHistoryElement.value; // '<a href="' + scannedText + '" target="_blank">' + scannedText + '</a>\n' + scanHistoryElement.innerHTML;
        }
    }

    _clipboardTimerCallback() {
        if (!this.execTimerCallback) {
            return;
        }
        this.clipboardButton.classList.toggle("confirm");
        this.execTimerCallback = false;
        this.clipboardButton.title = '';
        clearInterval(interval);
        this._toggleClipboardTooltip();
    }

    _displayBlock(element, show = true) {
        if (!element) {
            return;
        }
        element.style.display = show ? 'block' : 'none';
    }

    _getCodeBlock(scannedText) {
        return '<code class="modal-code-url">' + scannedText + '</code>';
    }

    _updateDevicePermissions() {
        let self = this;
        navigator.mediaDevices.getUserMedia({audio: false, video: true})
            .then(function (stream) {
                self.hasPermission = true;
                self._updateHint();
            })
            .catch(function (error) {
                self.enableDebugMode();
                self._updateHint(error);
            });
    }

    _getLastElementAdded() {
        let scanResults = this.scanResults || [];
        if (scanResults == null) {
            logDebug('scanResults is null');
        }
        return scanResults.length > 0 ? scanResults[scanResults.length - 1].value : "";
    }

    _getModalMsgUrlInnerHtml(scannedText) {
        return '<a href="' + scannedText + '" target="_blank">' + this._getCodeBlock(scannedText) + '</a>';
    }

    _getNavigateModal() {
        return $('#navigate-modal');
    }

    _isValidURL(str) {
        let regex = /(?:https?):\/\/(\w+:?\w*)?(\S+)(:\d+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
        return regex.test(str);
    }

    _setScannedText(value) {
        if (this.lastScannedElement) {
            this.lastScannedElement.value = value;
            this.lastScannedText = value;
        }
    }

    _showModal(show = true) {
        let value = show ? 'show' : 'hide';
        this._getNavigateModal().modal(value);
        this.showNavigateModal = show;
    }

    _showModalIfUrl(scannedText) {
        if (this.showNavigateModal || !this._isValidURL(scannedText)) {
            return;
        }

        this._updateModalBody(scannedText);
        this._updateANavigateUrl(scannedText);
        this._showModal();
    }

    _toggleClipboardTooltip() {
        // TODO show tooltip on top of button
        // $('#clipboard-button').tooltip({
        //     if(showTitle) {
        //         $('[data-bs-toggle="tooltip"]').tooltip(tooltip).show();
        //     } else {
        //         $('[data-bs-toggle="tooltip"]').tooltip(tooltip).hide();
        // }
        const addendum = " to clipboard";
        if (this.showTitle) {
            this.clipboardButton.title = "Copied" + addendum;
        } else {
            this.clipboardButton.title = "Copy" + addendum;
        }
        // console.log('element.title = ' + clipboardButton.title);
        this.showTitle = !this.showTitle;
        // element.attributes['data-toggle'] = 'tooltip';
    }

    _showUpdatedHistoryBlock(historyCount) {
        if (historyCount > 1) {
            this._updateHistoryLabel(historyCount); // <old length> + 1 = new length
            this._displayBlock(this.scanHistoryDiv);
        }
    }

    _updateHistoryLabel(historyCount) {
        if (this.scanHistoryLabel) {
            this.scanHistoryLabel.innerText = "Scan history (" + historyCount + ")";
        }
    }

    _updateANavigateUrl(scannedText) {
        return $('#a-navigate').prop("href", scannedText);
    }

    _updateModalBody(scannedText) {
        this.modalMsgHeader.textContent = "The scanned text is an URL:";
        this.modalMsgUrl.innerHTML = this._getModalMsgUrlInnerHtml(scannedText);
        this.modalMsgFooter.textContent = "Visit?";
    }

    _updateHint(customError = null) {
        if (this.hasPermission) {
            this.hintMsgSpan.innerText = "Point the camera to a QR code to scan it";
        } else {
            let split = customError != null ? String(customError).split(': ') : [];
            let prefix = customError != null ? split[0] + ": " : "Error: ";
            let message = customError != null ? split[1] : "Refresh and allow camera access to scan QR codes";
            this.hintPrefixSpan.innerText = prefix;
            this.hintMsgSpan.innerText = message;
        }
    }
}

class Controller {
    constructor(model, view) {
        this.model = model;
        this.view = view;

        this.model.bindScanResultsChanged(this.onScanResultsChanged);
        this.view.bindAddScanResult(this.handleAddScanResult);
        this.onScanResultsChanged(this.model.scanResults);
    }

    static getInstance() {
        if (Controller._instance == null) {
            Controller._instance = new Controller(new Model(), new View());
        }
        return Controller._instance;
    }

    copyToClipboard = () => {
        this.view.copyToClipboard();
    }

    enableDebugMode = () => {
        this.view.enableDebugMode();
    }

    onCodeScan = scannedText => {
        this.view.setScanResults(this.model.scanResults);
        if (this.view.onCodeScan(scannedText)) {
            this.handleAddScanResult(scannedText);
        }
    }

    onScanResultsChanged = scanResults => {
        this.view.setScanResults(scanResults);
        this.view.displayScanResults(scanResults);
    }

    handleAddScanResult = scannedText => {
        // Push new object to list
        this.model.addScanResult(scannedText);
        this.onScanResultsChanged(this.model.scanResults);
    }

    prepareUi = () => {
        this.view.prepareUi(this.model.scanResults);
    }
}

function getController() {
    return Controller.getInstance();
}

function getLogPrefix(loglevel) {
    return '[' + _getFormattedDateTimeStamp(new Date()) + ' ' + loglevel + ']: ';
}

function log(loglevel, message, isError) {
    let logFunction = isError ? console.error : console.log;
    logFunction(getLogPrefix(loglevel) + message);
}

function logDebug(message) {
    return log("DEBUG", message, false);
}

function logError(message) {
    alert("[DEBUG/Error]: " + message);
    return log("ERROR", message, true);
}

function logInfo(message) {
    return log("INFO", message, false);
}

function logWarn(message) {
    return log("WARN", message, false);
}

function _getFormattedDateStamp(date) {
    let result = '';
    result += date.getFullYear() + '-';
    result += _padValue(date.getMonth() + 1) + '-';
    result += _padValue(date.getDate() + 1);
    return result;
}

function _getFormattedDateTimeStamp(date) {
    return _getFormattedDateStamp(date) + ' ' + _getFormattedTimeStamp(date);
}

function _getFormattedTimeStamp(date) {
    let result = '';
    result += _padValue(date.getHours()) + ':';
    result += _padValue(date.getMinutes()) + ':';
    result += _padValue(date.getSeconds());
    return result;
}

function _padValue(value) {
    return String(value).padStart(2, '0');
}
